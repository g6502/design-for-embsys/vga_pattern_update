	component new_core is
		port (
			clk_clk                               : in  std_logic                     := 'X';             -- clk
			reset_reset_n                         : in  std_logic                     := 'X';             -- reset_n
			vga_out_clocked_video_vid_clk         : in  std_logic                     := 'X';             -- vid_clk
			vga_out_clocked_video_vid_data        : out std_logic_vector(11 downto 0);                    -- vid_data
			vga_out_clocked_video_underflow       : out std_logic;                                        -- underflow
			vga_out_clocked_video_vid_mode_change : out std_logic;                                        -- vid_mode_change
			vga_out_clocked_video_vid_std         : out std_logic;                                        -- vid_std
			vga_out_clocked_video_vid_datavalid   : out std_logic;                                        -- vid_datavalid
			vga_out_clocked_video_vid_v_sync      : out std_logic;                                        -- vid_v_sync
			vga_out_clocked_video_vid_h_sync      : out std_logic;                                        -- vid_h_sync
			vga_out_clocked_video_vid_f           : out std_logic;                                        -- vid_f
			vga_out_clocked_video_vid_h           : out std_logic;                                        -- vid_h
			vga_out_clocked_video_vid_v           : out std_logic;                                        -- vid_v
			switches_external_connection_export   : in  std_logic_vector(2 downto 0)  := (others => 'X')  -- export
		);
	end component new_core;

	u0 : component new_core
		port map (
			clk_clk                               => CONNECTED_TO_clk_clk,                               --                          clk.clk
			reset_reset_n                         => CONNECTED_TO_reset_reset_n,                         --                        reset.reset_n
			vga_out_clocked_video_vid_clk         => CONNECTED_TO_vga_out_clocked_video_vid_clk,         --        vga_out_clocked_video.vid_clk
			vga_out_clocked_video_vid_data        => CONNECTED_TO_vga_out_clocked_video_vid_data,        --                             .vid_data
			vga_out_clocked_video_underflow       => CONNECTED_TO_vga_out_clocked_video_underflow,       --                             .underflow
			vga_out_clocked_video_vid_mode_change => CONNECTED_TO_vga_out_clocked_video_vid_mode_change, --                             .vid_mode_change
			vga_out_clocked_video_vid_std         => CONNECTED_TO_vga_out_clocked_video_vid_std,         --                             .vid_std
			vga_out_clocked_video_vid_datavalid   => CONNECTED_TO_vga_out_clocked_video_vid_datavalid,   --                             .vid_datavalid
			vga_out_clocked_video_vid_v_sync      => CONNECTED_TO_vga_out_clocked_video_vid_v_sync,      --                             .vid_v_sync
			vga_out_clocked_video_vid_h_sync      => CONNECTED_TO_vga_out_clocked_video_vid_h_sync,      --                             .vid_h_sync
			vga_out_clocked_video_vid_f           => CONNECTED_TO_vga_out_clocked_video_vid_f,           --                             .vid_f
			vga_out_clocked_video_vid_h           => CONNECTED_TO_vga_out_clocked_video_vid_h,           --                             .vid_h
			vga_out_clocked_video_vid_v           => CONNECTED_TO_vga_out_clocked_video_vid_v,           --                             .vid_v
			switches_external_connection_export   => CONNECTED_TO_switches_external_connection_export    -- switches_external_connection.export
		);

