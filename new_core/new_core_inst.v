	new_core u0 (
		.clk_clk                               (<connected-to-clk_clk>),                               //                          clk.clk
		.reset_reset_n                         (<connected-to-reset_reset_n>),                         //                        reset.reset_n
		.vga_out_clocked_video_vid_clk         (<connected-to-vga_out_clocked_video_vid_clk>),         //        vga_out_clocked_video.vid_clk
		.vga_out_clocked_video_vid_data        (<connected-to-vga_out_clocked_video_vid_data>),        //                             .vid_data
		.vga_out_clocked_video_underflow       (<connected-to-vga_out_clocked_video_underflow>),       //                             .underflow
		.vga_out_clocked_video_vid_mode_change (<connected-to-vga_out_clocked_video_vid_mode_change>), //                             .vid_mode_change
		.vga_out_clocked_video_vid_std         (<connected-to-vga_out_clocked_video_vid_std>),         //                             .vid_std
		.vga_out_clocked_video_vid_datavalid   (<connected-to-vga_out_clocked_video_vid_datavalid>),   //                             .vid_datavalid
		.vga_out_clocked_video_vid_v_sync      (<connected-to-vga_out_clocked_video_vid_v_sync>),      //                             .vid_v_sync
		.vga_out_clocked_video_vid_h_sync      (<connected-to-vga_out_clocked_video_vid_h_sync>),      //                             .vid_h_sync
		.vga_out_clocked_video_vid_f           (<connected-to-vga_out_clocked_video_vid_f>),           //                             .vid_f
		.vga_out_clocked_video_vid_h           (<connected-to-vga_out_clocked_video_vid_h>),           //                             .vid_h
		.vga_out_clocked_video_vid_v           (<connected-to-vga_out_clocked_video_vid_v>),           //                             .vid_v
		.switches_external_connection_export   (<connected-to-switches_external_connection_export>)    // switches_external_connection.export
	);

