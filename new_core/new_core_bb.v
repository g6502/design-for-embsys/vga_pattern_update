
module new_core (
	clk_clk,
	reset_reset_n,
	vga_out_clocked_video_vid_clk,
	vga_out_clocked_video_vid_data,
	vga_out_clocked_video_underflow,
	vga_out_clocked_video_vid_mode_change,
	vga_out_clocked_video_vid_std,
	vga_out_clocked_video_vid_datavalid,
	vga_out_clocked_video_vid_v_sync,
	vga_out_clocked_video_vid_h_sync,
	vga_out_clocked_video_vid_f,
	vga_out_clocked_video_vid_h,
	vga_out_clocked_video_vid_v,
	switches_external_connection_export);	

	input		clk_clk;
	input		reset_reset_n;
	input		vga_out_clocked_video_vid_clk;
	output	[11:0]	vga_out_clocked_video_vid_data;
	output		vga_out_clocked_video_underflow;
	output		vga_out_clocked_video_vid_mode_change;
	output		vga_out_clocked_video_vid_std;
	output		vga_out_clocked_video_vid_datavalid;
	output		vga_out_clocked_video_vid_v_sync;
	output		vga_out_clocked_video_vid_h_sync;
	output		vga_out_clocked_video_vid_f;
	output		vga_out_clocked_video_vid_h;
	output		vga_out_clocked_video_vid_v;
	input	[2:0]	switches_external_connection_export;
endmodule
